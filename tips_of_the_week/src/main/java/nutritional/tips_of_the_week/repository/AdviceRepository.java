package nutritional.tips_of_the_week.repository;

import nutritional.tips_of_the_week.domain.Advice;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AdviceRepository extends CrudRepository<Advice, Integer> {
    public List<Advice> findAllByOrderByDateDesc();
}
