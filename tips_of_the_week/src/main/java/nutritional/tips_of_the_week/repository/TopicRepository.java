package nutritional.tips_of_the_week.repository;

import nutritional.tips_of_the_week.domain.Topic;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface TopicRepository extends CrudRepository<Topic, Integer> {
    public List<Topic> findAll();
}
