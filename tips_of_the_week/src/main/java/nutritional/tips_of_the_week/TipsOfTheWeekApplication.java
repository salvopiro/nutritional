package nutritional.tips_of_the_week;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;


@EnableEurekaClient
@SpringBootApplication
public class TipsOfTheWeekApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(TipsOfTheWeekApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(TipsOfTheWeekApplication.class, args);
	}

	public static Logger getLogger(){
		if(LOGGER != null)
			return LOGGER;
		return null;
	}


}
