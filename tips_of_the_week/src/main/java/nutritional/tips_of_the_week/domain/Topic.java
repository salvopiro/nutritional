package nutritional.tips_of_the_week.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Builder(toBuilder = true)
public class Topic {
    @Id
    @GeneratedValue
    @Column
    private int id;

    private String name;

    @OneToMany(mappedBy = "topic")
    private List<Advice> advices;
}
