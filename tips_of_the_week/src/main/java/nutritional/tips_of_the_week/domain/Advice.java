package nutritional.tips_of_the_week.domain;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Data
@Builder(toBuilder = true)
@AllArgsConstructor
@NoArgsConstructor
@Entity
public class Advice {
    @Id
    @GeneratedValue
    @Column
    private int id;

    @JsonIgnore
    @ManyToOne
    private Topic topic;

    private String advice;

    private LocalDate date;
}
