package nutritional.tips_of_the_week.service;

import nutritional.tips_of_the_week.domain.Topic;
import nutritional.tips_of_the_week.repository.TopicRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TopicService {

    private TopicRepository topicRepository;

    @Autowired
    public TopicService(TopicRepository topicRepository){
        this.topicRepository = topicRepository;
    }

    public List<Topic> getAllTopic(){
        return topicRepository.findAll();
    }

    public Optional<Topic> getTopic(int id){
        return topicRepository.findById(id);
    }

    public Topic postTopic(Topic topic){
        return topicRepository.save(topic);
    }

}
