package nutritional.tips_of_the_week.service;


import nutritional.tips_of_the_week.domain.Advice;
import nutritional.tips_of_the_week.event.Event;
import nutritional.tips_of_the_week.event.EventDispatcher;
import nutritional.tips_of_the_week.repository.AdviceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AdviceService {


    private AdviceRepository adviceRepository;

    private EventDispatcher eventDispatcher;

    @Autowired
    public AdviceService(AdviceRepository adviceRepository, EventDispatcher eventDispatcher){
        this.adviceRepository = adviceRepository;
        this.eventDispatcher = eventDispatcher;
    }

    public List<Advice> getAllAdvice(){
        return adviceRepository.findAllByOrderByDateDesc();
    }

    public Optional<Advice> getAdvice(int id){
        return adviceRepository.findById(id);
    }

    public Advice postAdvice(Advice advice){
        Advice adviceSaved =adviceRepository.save(advice);
        eventDispatcher.send(new Event(adviceSaved.getAdvice(), "new advice"));
        return adviceSaved;
    }
}
