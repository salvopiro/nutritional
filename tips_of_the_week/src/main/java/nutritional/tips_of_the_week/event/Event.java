package nutritional.tips_of_the_week.event;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Event{
    private String text;
    private String subject;
//    public Event(String text){
//        this.text = text;
//    }


}
