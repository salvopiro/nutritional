package nutritional.tips_of_the_week.controller;


import nutritional.tips_of_the_week.domain.Advice;
import nutritional.tips_of_the_week.errors.AdviceNotFoundException;
import nutritional.tips_of_the_week.service.AdviceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/advice")
public class AdviceController {


    private AdviceService adviceService;

    @Autowired
    public AdviceController (AdviceService adviceService){
        this.adviceService = adviceService;
    }

    @GetMapping("/all_advice/")
    public List<Advice> getAllAdvice(){
        return adviceService.getAllAdvice();
    }


    @GetMapping("/advice/{id}")
    public Advice getAdvice(@PathVariable int id){
        return adviceService.getAdvice(id).orElseThrow(AdviceNotFoundException::new);
    }

    @PostMapping("/new/advice")
    @ResponseStatus(HttpStatus.CREATED)
    public Advice postAdvice(@RequestBody Advice advice){
        return adviceService.postAdvice(advice);
    }
}
