package nutritional.tips_of_the_week.controller;


import nutritional.tips_of_the_week.domain.Topic;
import nutritional.tips_of_the_week.errors.TopicNotFoundException;
import nutritional.tips_of_the_week.service.TopicService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
//@RequestMapping("/topic")
public class TopicController {

    @Autowired
    TopicService topicService;

    @GetMapping("/all_topic")
    public List<Topic> getAllTopic(){
        return topicService.getAllTopic();
    }

    @GetMapping("/topic/{id}")
    public Topic getTopic(@PathVariable int id){
        return topicService.getTopic(id).orElseThrow(TopicNotFoundException::new);
    }

    @PostMapping("/new/topic/")
    @ResponseStatus(HttpStatus.CREATED)
    public Topic postTopic(@RequestBody Topic topic){
        return topicService.postTopic(topic);
    }
}
