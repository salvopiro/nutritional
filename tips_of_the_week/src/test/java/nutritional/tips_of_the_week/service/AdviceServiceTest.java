package nutritional.tips_of_the_week.service;

import nutritional.tips_of_the_week.domain.Advice;
import nutritional.tips_of_the_week.domain.Topic;
import nutritional.tips_of_the_week.repository.AdviceRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
public class AdviceServiceTest {
    @Mock
    AdviceRepository adviceRepository;

    @InjectMocks
    AdviceService adviceService;

    @Test
    public void getAdviceShouldReturnAdvice(){
        Advice advice = validAdvice(1);

        when(adviceRepository.findById(1)).thenReturn(Optional.of(advice));

        assertEquals(advice, adviceService.getAdvice(1).get());
    }

    @Test
    public void getAllAdviceShouldReturnAdvices(){
        Advice advice1 = validAdvice(1), advice2 = validAdvice(2);
        List<Advice> adviceList = new ArrayList<Advice>();
        adviceList.add(advice1);
        adviceList.add(advice2);

        when(adviceRepository.findAllByOrderByDateDesc()).thenReturn(adviceList);
        assertEquals(adviceList, adviceService.getAllAdvice());

    }

    @Test
    public void getAdviceShouldNotReturnAdvice(){
        when(adviceRepository.findById(1)).thenReturn(Optional.empty());
        assertTrue(adviceService.getAdvice(1).isEmpty());
    }



    private Advice validAdvice(int id){
        LocalDate date = LocalDate.now();
        return Advice.builder()
                .id(id)
                .advice("Consiglio numero 1")
                .topic(new Topic())
                .date(date)
                .build();
    }
    private Advice validAdvice(){
        LocalDate date = LocalDate.now();

        return Advice.builder()
                .advice("Consiglio numero 1")
                .date(date)
                .build();
    }


}
