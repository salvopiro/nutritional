package nutritional.tips_of_the_week.service;

import nutritional.tips_of_the_week.domain.Topic;
import nutritional.tips_of_the_week.repository.TopicRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(MockitoExtension.class)
public class TopicServiceTest {
    @Mock TopicRepository topicRepository;

    @InjectMocks TopicService topicService;

    @Test
    public void getTopicShouldReturnTopic(){
        Topic topic = validTopic(1);
        when(topicRepository.findById(1)).thenReturn(Optional.of(topic));
        assertEquals(topic, topicService.getTopic(1).get());
    }

    @Test
    public void getTopicShouldNotReturnTopicIfNotFound(){
        when(topicRepository.findById(1)).thenReturn(Optional.empty());
        assertEquals(topicService.getTopic(1), Optional.empty());
    }

    @Test
    public void getAllTopicShouldReturnAllTopic(){
        Topic topic1 = validTopic(1), topic2 = validTopic(2);
        List<Topic> topicList = new ArrayList<Topic>();

        topicList.add(topic1);
        topicList.add(topic2);

        when(topicRepository.findAll()).thenReturn(topicList);

        assertEquals(topicService.getAllTopic(), topicList);
    }

    @Test
    public void getAllTopicShouldReturnEmptyArrayIfNotFoundTopic(){
        when(topicRepository.findAll()).thenReturn(new ArrayList<Topic>());
        assertTrue(topicService.getAllTopic().isEmpty());
    }
    private Topic validTopic(int id){
        return Topic.builder()
                .id(id)
                .name("www")
                .build();
    }
}
