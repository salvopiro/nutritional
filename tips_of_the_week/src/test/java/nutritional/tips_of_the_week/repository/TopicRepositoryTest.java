package nutritional.tips_of_the_week.repository;


import nutritional.tips_of_the_week.domain.Topic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class TopicRepositoryTest {

    @Autowired TopicRepository topicRepository;

    @Test
    public void findByIdShouldReturnTopic(){
        Topic topic = validTopic(1);
        Topic expectedTopic = topicRepository.save(topic);
        assertEquals(topicRepository.findById(1).get(), expectedTopic);
    }

    @Test
    public void findByIdShouldNotReturnTopic(){
        assertEquals(topicRepository.findById(1), Optional.empty());
    }

    @Test
    public void findAllShouldReturnListOfTopic(){
        Topic topic1 = validTopic(1), topic2 = validTopic(2);
        List<Topic> topicList = new ArrayList<Topic>();
        topicList.add(topicRepository.save(topic1));
        topicList.add(topicRepository.save(topic2));
        assertEquals(topicRepository.findAll(), topicList);
    }

    @Test
    public void findAllShouldReturnEmptyList(){
        assertEquals(topicRepository.findAll(), new ArrayList<Topic>());
    }

    private Topic validTopic(int id){
        return Topic.builder()
                .id(id)
                .name("www")
                .build();
    }
}
