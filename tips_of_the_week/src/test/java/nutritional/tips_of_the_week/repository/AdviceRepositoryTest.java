package nutritional.tips_of_the_week.repository;

import nutritional.tips_of_the_week.domain.Advice;
import nutritional.tips_of_the_week.domain.Topic;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class AdviceRepositoryTest {

    @Autowired AdviceRepository adviceRepository;

    @Autowired
    TopicRepository topicRepository;

    @Test
    public void findByIdShouldReturnAdvice(){
        Advice advice = validAdvice(1);
        Advice expectedAdvice = adviceRepository.save(advice);

        assertEquals(expectedAdvice, adviceRepository.findById(1).get());

    }

    @Test
    void findByIdShouldReturnEmptyIfAdviceNotFound(){
        assertEquals(
                Optional.empty(),
                adviceRepository.findById(1250)
        );
    }

    @Test
    public void findAllByOrderByDateDescShouldReturnAdvice(){
        topicRepository.save(validTopic());
        Advice advice1 = validAdvice(), advice2 = validAdvice();
        List<Advice> adviceList = new ArrayList<Advice>();

        Advice adviceExpected1 = adviceRepository.save(advice1);
        Advice adviceExpected2 = adviceRepository.save(advice2);

        adviceList.add(adviceExpected1);
        adviceList.add(adviceExpected2);

        assertEquals(adviceList, adviceRepository.findAllByOrderByDateDesc());

    }

    private Advice validAdvice(int id){
        LocalDate date = LocalDate.now();
        return Advice.builder()
                .id(id)
                .advice("Consiglio numero 1")
                .topic(topicRepository.findById(1).get())
                .date(date)
                .build();
    }
    private Advice validAdvice(){
        LocalDate date = LocalDate.now();

        return Advice.builder()
                .advice("Consiglio numero 1")
                .date(date)
                .topic(topicRepository.findById(1).get())
                .build();
    }

    private Topic validTopic(){
        return Topic.builder()
                .id(1)
                .name("www")
                .build();
    }

}
