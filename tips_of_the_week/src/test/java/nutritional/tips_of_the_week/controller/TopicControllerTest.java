package nutritional.tips_of_the_week.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nutritional.tips_of_the_week.domain.Topic;
import nutritional.tips_of_the_week.service.TopicService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(TopicController.class)
public class TopicControllerTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mvc;

    @MockBean
    TopicService topicService;

    @Test
    public void getTopicShouldReturnTopic()throws Exception{
        int topicId = 1;
        Topic topic = validTopic(topicId);

        when(topicService.getTopic(topicId)).thenReturn(Optional.of(topic));

        mvc.perform(get("/topic/"+topicId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(topicId)))
                .andExpect(jsonPath("$.name", is(topic.getName())));
    }

    @Test
    public void getTopicShouldNotReturnTopicIfNotFound()throws Exception{
        when(topicService.getTopic(1)).thenReturn(Optional.empty());

        mvc.perform(get("/topic/1"))
                .andExpect(status().isNotFound());
    }

    @Test
    public void getAllTopicShouldReturnTopicsIfIsPresent() throws Exception{
        Topic topic1 = validTopic(1), topic2 = validTopic(2);
        List<Topic> topicList = new ArrayList<Topic>();
        topicList.add(topic1);
        topicList.add(topic2);

        when(topicService.getAllTopic()).thenReturn(topicList);

        mvc.perform(get("/topics"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(1)))
                .andExpect(jsonPath("$.[1].id", is(2)));
    }

    @Test
    public void getAllTopicShouldReturnEmptyArrayIfNotFoundTopic()throws Exception{
        when(topicService.getAllTopic()).thenReturn(new ArrayList<Topic>());
        mvc.perform(get("/topics"))
                .andExpect(jsonPath("$").isEmpty());
    }

    @Test
    public void postTopicShouldSaveNewTopic()throws Exception{
        Topic topic = validTopic();

        when(topicService.postTopic(topic)).thenReturn(topic.toBuilder().id(0).build());

        mvc.perform(post("/new_topic")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(topic)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(topic.getId())))
                .andExpect(jsonPath("$.name", is(topic.getName())));
    }

    private Topic validTopic(int id){
        return Topic.builder()
                .id(id)
                .name("www")
                .build();
    }
    private Topic validTopic(){
        return Topic.builder()
                .name("www")
                .build();
    }
}
