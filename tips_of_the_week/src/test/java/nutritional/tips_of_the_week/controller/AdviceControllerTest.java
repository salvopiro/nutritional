package nutritional.tips_of_the_week.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import nutritional.tips_of_the_week.domain.Advice;
import nutritional.tips_of_the_week.domain.Topic;
import nutritional.tips_of_the_week.service.AdviceService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(MockitoExtension.class)
@WebMvcTest(AdviceController.class)
public class AdviceControllerTest {

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    MockMvc mvc;

    @MockBean
    AdviceService adviceService;

    @Test
    public void getAllAdviceShouldReturnAdvices() throws Exception{
        int adviceId1 = 1, adviceId2 = 2;
        Advice advice1 = validAdvice(adviceId1);
        Advice advice2 = validAdvice(adviceId2);

        List<Advice> adviceList = new ArrayList<Advice>();
        adviceList.add(advice1);
        adviceList.add(advice2);

        when(adviceService.getAllAdvice()).thenReturn(adviceList);

        mvc.perform(get("/advices"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(adviceId1)))
                .andExpect(jsonPath("$.[1].id", is(adviceId2)));

    }

    @Test
    public void getAdviceShouldReturnAdvice()throws Exception{
        int adviceId = 1;
        Advice advice = validAdvice(adviceId);

        when(adviceService.getAdvice(adviceId)).thenReturn(Optional.of(advice));

        mvc.perform(get("/advice/"+adviceId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(adviceId)))
                .andExpect(jsonPath("$.advice", is(advice.getAdvice())))
                .andExpect(jsonPath("$.topic.id", is(advice.getTopic().getId())));
//              .andExpect(jsonPath("$.date", is(advice.getDate())));
    }


    @Test
    public void postAdviceShouldSaveAdvice()throws Exception{
        int adviceId = 1;
        Advice advice = validAdvice();
        when(adviceService.postAdvice(advice)).thenReturn(advice.toBuilder().id(adviceId).build());

        mvc.perform(post("/new_advice")
        .contentType(MediaType.APPLICATION_JSON)
        .content(objectMapper.writeValueAsBytes(advice)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(adviceId)))
                .andExpect(jsonPath("$.advice", is(advice.getAdvice())));
//              .andExpect(jsonPath("$.date", is(advice.getDate())));
//              .andExpect(jsonPath("$.topic.id", is(advice.getTopic().getId())));

    }

    private Advice validAdvice(int id){
        LocalDate date = LocalDate.now();
        return Advice.builder()
                .id(id)
                .advice("Consiglio numero 1")
                .topic(new Topic())
                .date(date)
                .build();
    }
    private Advice validAdvice(){
        LocalDate date = LocalDate.now();

        return Advice.builder()
                .advice("Consiglio numero 1")
                .date(date)
                .build();
    }
}
