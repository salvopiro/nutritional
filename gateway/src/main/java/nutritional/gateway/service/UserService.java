package nutritional.gateway.service;


import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import lombok.Data;
import nutritional.gateway.config.RestTemplateImpl;
import nutritional.gateway.domain.Role;
import nutritional.gateway.domain.RoleName;
import nutritional.gateway.domain.UserAuthentication;
import nutritional.gateway.domain.UserPrinciple;
import nutritional.gateway.exception.UserNotFoundException;
import nutritional.gateway.repository.RoleRepository;
import nutritional.gateway.repository.UserRepository;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;


import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

import org.slf4j.Logger;


@Service
public class UserService implements UserDetailsService {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserService.class);

    @Autowired
    RestTemplateImpl restTemplate;

    @Autowired
    UserRepository userRepository;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    RoleRepository roleRepository;

    @Autowired
    private EurekaClient discoveryClient;

    private String serviceUrl() {
        InstanceInfo instance = discoveryClient.getNextServerFromEureka("ANTHROPOMETRIC_DATA", false);
        System.out.println(instance.getHomePageUrl());
        return instance.getHomePageUrl();
    }

    public boolean existsPatientByEmail(String email){

        ResponseEntity<Boolean> responseEntity = restTemplate.getForEntity(
                    serviceUrl()+"exists_patient_email/"+ email, //http://localhost:8080/exists_patient_email/"+ email,/
                    Boolean.class);
        return responseEntity.getBody().booleanValue();
    }


    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
         UserAuthentication user = userRepository.findByEmail(email).orElseThrow(UserNotFoundException::new);

         return UserPrinciple.build(user);
    }

    public int getIdFromEmail(String email) throws IOException {
        ResponseEntity<String> responseEntity = restTemplate.getForEntity(
                serviceUrl()+"patient_email/"+ email, //"http://anthropometric_data/patient_email/"+ email,
                String.class);

        String jsonPatient = responseEntity.getBody();
        ObjectNode node = objectMapper.readValue(jsonPatient, ObjectNode.class);
        if(node.has("id"))
            return node.get("id").asInt();
        return -1;
    }

    public List<String> getUsersEmail(){
        List<UserAuthentication> listUser = userRepository.findByRoles(
                roleRepository.findByName(RoleName.ROLE_USER).orElseThrow(()-> new RuntimeException("Role not found"))
        ).orElseThrow(()-> new RuntimeException("UserNot present"));

        return listUser.stream().map(UserAuthentication::getEmail).collect(Collectors.toList());
    }

    public UserAuthentication saveUser(UserAuthentication user, RoleName roleName){
        List<Role> roles = new ArrayList<>();
        Role userRole = roleRepository.findByName(roleName).orElseThrow(()-> new RuntimeException("Role not found"));
        roles.add(userRole);
        user.setRoles(roles);
        return userRepository.save(user);
    }
}
