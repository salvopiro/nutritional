//package nutritional.gateway.service;
//
//import com.fasterxml.jackson.databind.ObjectMapper;
//import com.fasterxml.jackson.databind.node.ObjectNode;
//import com.jayway.jsonpath.JsonPath;
//import com.jayway.jsonpath.ReadContext;
//import nutritional.gateway.domain.UserAuthentication;
//import nutritional.gateway.domain.UserPrinciple;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.core.userdetails.UserDetails;
//import org.springframework.security.core.userdetails.UserDetailsService;
//import org.springframework.security.core.userdetails.UsernameNotFoundException;
//import org.springframework.stereotype.Service;
//
//import java.io.IOException;
//
//@Service
//public class UserAuthenticationService implements UserDetailsService {
//
//    @Autowired
//    UserService userService;
//
//    @Autowired
//    ObjectMapper objectMapper;
//
////    @Autowired
////    JsonPath jsonPath;
//
//    @Override
//    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
//        return null;
//    }
//
//    public UserDetails loadById(int id) throws IOException {
//
//        String jsonUser = userService.getJsonPatient(id);
//        ObjectNode node = objectMapper.readValue(jsonUser, ObjectNode.class);
//        UserAuthentication user = new UserAuthentication();
//
//        if(node.has("id"))
//            user.setIdPatient(node.get("id").asInt());
//        if(node.has("email"))
//            user.setEmail(node.get("email").asText());
//        if(node.has("password"))
//            user.setPassword(node.get("password").asText());
//        if(node.has("name"))
//            user.setName(node.get("name").asText());
//        if(node.has("surname"))
//            user.setSurname(node.get("surname").asText());
//        //ReadContext readContext = jsonPath.parse(jsonUser);
//
//
////        user.setIdPatient(readContext.read("$.id", Integer.class));
////        user.setEmail(readContext.read("$.email"));
////        user.setPassword(readContext.read("$.password"));
////        user.setName(readContext.read("$.name"));
////        user.setSurname(readContext.read("$.surname"));
//
//        return UserPrinciple.build(user);
//    }
//}
