package nutritional.gateway.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.AllArgsConstructor;
import nutritional.gateway.GatewayApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;


@AllArgsConstructor
@Service
public class AmazonS3Service {

    @Autowired
    private final AmazonS3 amazonS3;

    @Scheduled(cron = "00 17 15 * * ?", zone = "Europe/Rome")
    public void upload(){
        System.out.println("Procedure upload log.....");
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String today = formatter.format(date);

        File f = new File("./log/gateway_application.log");
        File copied = new File("./log/gateway_application"+today+".log");
        try {


            InputStream in = new BufferedInputStream(new FileInputStream(f));
            OutputStream out = new BufferedOutputStream(new FileOutputStream(copied));

            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }

            amazonS3.putObject(new PutObjectRequest("logprogettosc", "gateway_application/gateway_"+today+".log", copied));
        } catch (Exception e) {
            System.out.println(e.getMessage());
            GatewayApplication.getLogger().error("Failed to upload the file", e);
        }
        copied.delete();

    }

}
