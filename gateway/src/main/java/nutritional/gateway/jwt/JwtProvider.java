package nutritional.gateway.jwt;


import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.MalformedJwtException;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.SignatureException;
import nutritional.gateway.GatewayApplication;
import nutritional.gateway.domain.UserPrinciple;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.security.core.Authentication;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;


@Component
public class JwtProvider {
    private final String jwtSecret = "jwtNutritionalAppSecret";
    private final int jwtExpiration = 86400;

    public String generateJwtToken(Authentication authentication){
        UserPrinciple userPrinciple = (UserPrinciple) authentication.getPrincipal();

        List<String> subject = new ArrayList<>();

        GatewayApplication.getLogger().info("Creating new JWT");
        return Jwts.builder()
                .setId(String.valueOf(userPrinciple.getId()))
                .setSubject((String.valueOf(userPrinciple.getEmail())))
                .claim("id", userPrinciple.getId())
                .setIssuedAt(new Date())
                .setExpiration(new Date((new Date()).getTime() + jwtExpiration * 1000))
                .signWith(SignatureAlgorithm.HS512, jwtSecret).compact();
    }

    public boolean validateJwtToken(String authToken){
        if(!(authToken == null || authToken.isEmpty()))
        try{
            Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(authToken);
            return true;
        } catch (Exception e){
            GatewayApplication.getLogger().error("Error message:{}", e.getMessage());
        }
        return false;
    }

    public String getEmailFromJwtToken(String jwtToken){
        return Jwts.parser().setSigningKey(jwtSecret).parseClaimsJws(jwtToken).getBody().getSubject();
    }

}
