package nutritional.gateway.domain;

public enum RoleName {
    ROLE_USER, ROLE_ADMIN
}
