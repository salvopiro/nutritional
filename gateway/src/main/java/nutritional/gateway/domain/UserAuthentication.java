package nutritional.gateway.domain;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import nutritional.gateway.domain.Role;
import org.hibernate.annotations.NaturalId;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
@Table(name = "users", uniqueConstraints = {@UniqueConstraint(columnNames = {"email"}) })
public class UserAuthentication {


    private int idPatient;
    //private String jwtToken;

    @Id
    @Size(max = 50)
    @Email
    @NotEmpty
    private String email;

    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id"))
    private List<Role> roles = new ArrayList<Role>();


    public UserAuthentication(int idPatient, String email, String password) {
        this.idPatient = idPatient;
        this.email = email;
        this.password = password;
    }

    public UserAuthentication(String email, String password) {

        this.email = email;
        this.password = password;
    }
}
