package nutritional.gateway.domain;

import lombok.Data;

@Data
public class LoginForm {
    private String email;
    private String password;
}
