package nutritional.gateway.domain;

import lombok.Data;

import javax.validation.constraints.Email;
import java.util.Set;

@Data
public class SignUpForm {

    private String password;
    @Email
    private String email;
    private Set<String> role;
}
