package nutritional.gateway.exception;

import nutritional.gateway.GatewayApplication;
import org.slf4j.Logger;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)

public class UserNotFoundException extends RuntimeException{
}
