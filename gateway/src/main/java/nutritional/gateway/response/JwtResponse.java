package nutritional.gateway.response;


import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.security.core.GrantedAuthority;

import java.util.Collection;

@Data

public class JwtResponse {
    private String token;
    private String type = "Bearer";
    private String email;
    private Collection<? extends GrantedAuthority> authorities;

    public JwtResponse(String token, String email, Collection<? extends GrantedAuthority> authorities){
        this.token = token;
        this.email = email;
        this.authorities = authorities;
    }
}
