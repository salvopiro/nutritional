package nutritional.gateway.controller;

import com.netflix.appinfo.InstanceInfo;
import com.netflix.discovery.EurekaClient;
import nutritional.gateway.GatewayApplication;
import nutritional.gateway.config.RestTemplateImpl;
import nutritional.gateway.domain.*;
import nutritional.gateway.jwt.JwtProvider;
import nutritional.gateway.repository.RoleRepository;
import nutritional.gateway.repository.UserRepository;
import nutritional.gateway.response.JwtResponse;
import nutritional.gateway.response.ResponseMessage;
import nutritional.gateway.domain.Role;
import nutritional.gateway.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge =  3600)
@RestController
public class AuthController {

    @Autowired
    AuthenticationManager authenticationManager;
    @Autowired
    UserRepository userRepository;

    @Autowired
    UserService userService;

    @Autowired
    RoleRepository roleRepository;

    @Autowired PasswordEncoder passwordEncoder;

    @Autowired JwtProvider jwtProvider;

    @Autowired
    RestTemplateImpl restTemplate;


    @PostMapping("/api/auth/signin")
    public ResponseEntity<?> authenticationUser(@Valid @RequestBody LoginForm login){

        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(login.getEmail(), login.getPassword()));

        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtProvider.generateJwtToken(authentication);
        UserDetails userDetails = (UserDetails) authentication.getPrincipal();

        return ResponseEntity.ok(new JwtResponse(jwt, userDetails.getUsername(), userDetails.getAuthorities()));
    }

    @PostMapping("/api/auth/signup")
    public ResponseEntity<?> registerUser(@Valid @RequestBody SignUpForm signUp) throws IOException {
        GatewayApplication.getLogger().info("Received signUp Request at: /api/auth/signup endpoint");
//        ResponseEntity<Boolean> responseEntity = restTemplate.getForEntity(
//                "http://localhost:8080/exists_patient_email/"+ signUp.getEmail(),
//                Boolean.class);


        if(!userService.existsPatientByEmail(signUp.getEmail())) {
            GatewayApplication.getLogger().info("User "+ signUp.getEmail() +" not already register from doctor");
            return new ResponseEntity<>(new ResponseMessage("User "+ signUp.getEmail() +" not already register from doctor"), HttpStatus.NOT_IMPLEMENTED);
        }
        if(userRepository.existsByEmail(signUp.getEmail())) {
            GatewayApplication.getLogger().info("Email "+ signUp.getEmail() +" already used");
            return new ResponseEntity<>(new ResponseMessage(("Email already used")), HttpStatus.BAD_REQUEST);
        }
        int idPatient = userService.getIdFromEmail(signUp.getEmail());

        UserAuthentication user = new UserAuthentication(idPatient, signUp.getEmail(), passwordEncoder.encode(signUp.getPassword()));

        UserAuthentication userSaved = userService.saveUser(user, RoleName.ROLE_USER);

        if(userSaved != null) {
            GatewayApplication.getLogger().info("User registered successfully");
            return new ResponseEntity<>(new ResponseMessage("User registered successfully"), HttpStatus.CREATED);
        }

        GatewayApplication.getLogger().info("User not registered successfully");
        return new ResponseEntity<>(new ResponseMessage("User not registered successfully"), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/api/auth/role_user")
    public ResponseEntity<?> newRole(){
        Role role= roleRepository.save(new Role(RoleName.ROLE_USER));
        if (role != null)
            return new ResponseEntity<>(new ResponseMessage("Role created succesfully"), HttpStatus.CREATED);
        return new ResponseEntity<>(new ResponseMessage("Role not created"), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/api/auth/role_admin")
    public ResponseEntity<?> newRoleAdmin(){
        Role role= roleRepository.save(new Role(RoleName.ROLE_ADMIN));
        if (role != null)
            return new ResponseEntity<>(new ResponseMessage("Role created succesfully"), HttpStatus.CREATED);
        return new ResponseEntity<>(new ResponseMessage("Role not created"), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/api/auth/admin")
    public ResponseEntity<?> postAdmin(@Valid @RequestBody SignUpForm signUp) {
        UserAuthentication user = new UserAuthentication(signUp.getEmail(), passwordEncoder.encode(signUp.getPassword()));

        UserAuthentication userSaved = userService.saveUser(user, RoleName.ROLE_ADMIN);
//        List<Role> roles = new ArrayList<>();
//        Role userRole = roleRepository.findByName(RoleName.ROLE_ADMIN).orElseThrow(()-> new RuntimeException("Role not found"));
//        roles.add(userRole);
//        user.setRoles(roles);
//        UserAuthentication userSaved = userRepository.save(user);
        if(userSaved != null)
            return new ResponseEntity<>(new ResponseMessage("User registered successfully"), HttpStatus.CREATED);
        return new ResponseEntity<>(new ResponseMessage("User not registered successfully"), HttpStatus.BAD_REQUEST);
    }

    @PostMapping("/api/auth/signup_test")
    public ResponseEntity<?> registerUserTest(@Valid @RequestBody SignUpForm signUp) throws IOException {
        UserAuthentication user = new UserAuthentication(signUp.getEmail(), passwordEncoder.encode(signUp.getPassword()));

        UserAuthentication userSaved = userService.saveUser(user, RoleName.ROLE_USER);

        if(userSaved != null)
            return new ResponseEntity<>(new ResponseMessage("User registered successfully"), HttpStatus.CREATED);
        return new ResponseEntity<>(new ResponseMessage("User not registered successfully"), HttpStatus.BAD_REQUEST);
    }
}
