package nutritional.gateway.repository;

import nutritional.gateway.domain.Role;
import nutritional.gateway.domain.UserAuthentication;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository  extends CrudRepository<UserAuthentication, Integer> {
    public Optional<UserAuthentication> findByEmail(String email);
    public boolean existsByEmail(String email);

    public Optional<List<UserAuthentication>> findByRoles(Role role);
}
