package nutritional.gateway.repository;

import nutritional.gateway.domain.Role;
import nutritional.gateway.domain.RoleName;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface RoleRepository extends CrudRepository<Role, Integer> {
    Optional<Role> findByName(RoleName role);
}
