package nutritional.gateway.config;

import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    @Bean
    public TopicExchange topicExchange(@Value("${exchange}") final String exchangeName){
        return new TopicExchange(exchangeName);
    }

    @Bean
    public Queue queueRequest(@Value("${mail.queue}") final String queueName){
        return new Queue(queueName, false);
    }


    @Bean
    public Binding binding(Queue queue, TopicExchange topicExchange, @Value("${request.routing-key}") String routingKey){
        return BindingBuilder.bind(queue).to(topicExchange).with(routingKey);
    }

    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }
}
