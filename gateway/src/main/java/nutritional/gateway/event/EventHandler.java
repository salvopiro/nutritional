package nutritional.gateway.event;

import jdk.jfr.Event;
import lombok.extern.slf4j.Slf4j;
import nutritional.gateway.GatewayApplication;
import nutritional.gateway.service.UserService;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;

@Slf4j
@Component
public class EventHandler {

    @Autowired
    private UserService userService;

    @Autowired
    private EventDispatcher eventDispatcher;

    @RabbitListener(queues = "${mail.queue}")
    void handleRequestUsersEmail(RequestUsersEmail request){
        GatewayApplication.getLogger().info("Ricevuta richiesta email user");
        ResponseUsersEmail responseUsersEmail = new ResponseUsersEmail(request.getSubject(), request.getText());
            //System.out.println("Messaggio ricevuto trovo tutti gli indirizzi email degli utenti");
        responseUsersEmail.setEmailUsersList(userService.getUsersEmail());

        eventDispatcher.send(responseUsersEmail);
    }


}
