package nutritional.gateway.event;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@Data
public class ResponseUsersEmail implements Serializable {

    private List<String> emailUsersList;

    private String subject;
    private String text;

    public ResponseUsersEmail(){

    }
    public ResponseUsersEmail(String subject, String text){
        this.subject = subject;
        this.text = text;
        emailUsersList= new ArrayList<>();
    }
}
