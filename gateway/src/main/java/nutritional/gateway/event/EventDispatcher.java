package nutritional.gateway.event;

import nutritional.gateway.GatewayApplication;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class EventDispatcher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    Binding bindingRequestMail;

    public void send (ResponseUsersEmail responseUsersEmail) {
        GatewayApplication.getLogger().info("Sending the emails of users at exchange: " + bindingRequestMail.getExchange());
        rabbitTemplate.convertAndSend(bindingRequestMail.getExchange(), bindingRequestMail.getRoutingKey(), responseUsersEmail);
    }

}
