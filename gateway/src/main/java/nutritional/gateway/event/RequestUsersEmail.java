package nutritional.gateway.event;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
public class RequestUsersEmail implements Serializable {


    private String subject;
    private String text;

    public RequestUsersEmail(){

    }
    public RequestUsersEmail(String subject, String text){
        this.subject = subject;
        this.text = text;

    }
}
