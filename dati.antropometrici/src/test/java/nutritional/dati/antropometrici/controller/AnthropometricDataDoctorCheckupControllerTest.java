package nutritional.dati.antropometrici.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import nutritional.dati.antropometrici.domain.AnthropometricDataDoctorCheckup;
import nutritional.dati.antropometrici.domain.AnthropometricDataPatientCheckup;
import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.service.AnthropometricDataDoctorCheckupService;
import nutritional.dati.antropometrici.service.PatientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(MockitoExtension.class)
@WebMvcTest(AnthropometricDataDoctorCheckupController.class)
public class AnthropometricDataDoctorCheckupControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AnthropometricDataDoctorCheckupService anthropometricDataDoctorCheckupService;

    @MockBean
    private PatientService patientService;

    @Autowired
    ObjectMapper objectMapper;

    @Test
    public void getShouldReturnData () throws Exception{
        int patientId = 1;
        Patient patient = validPatient(patientId);

        AnthropometricDataDoctorCheckup anthropometricDataDoctorCheckup1 = validData(patient);
        AnthropometricDataDoctorCheckup anthropometricDataDoctorCheckup2 = validData(patient);

        List<AnthropometricDataDoctorCheckup> dataToSave = new ArrayList<AnthropometricDataDoctorCheckup>();

        dataToSave.add(anthropometricDataDoctorCheckup1);
        dataToSave.add(anthropometricDataDoctorCheckup2);

        when(anthropometricDataDoctorCheckupService.get(patientId)).thenReturn(Optional.of(dataToSave));

        mvc.perform(get("/doctor_checkup/"+ patientId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(dataToSave.get(0).getId())))
                .andExpect(jsonPath("$.[0].patient.id", is(patientId)))
                .andExpect(jsonPath("$.[0].weight", is(dataToSave.get(0).getWeight())))
                .andExpect(jsonPath("$.[0].navelCircumference", is(dataToSave.get(0).getNavelCircumference())))
                .andExpect(jsonPath("$.[0].hipsWaistline", is(dataToSave.get(0).getHipsWaistline())))
                .andExpect(jsonPath("$.[0].waistCircumference", is(dataToSave.get(0).getWaistCircumference())))
                .andExpect(jsonPath("$.[0].thighDxCircumference", is(dataToSave.get(0).getThighDxCircumference())))
                .andExpect(jsonPath("$.[0].bmi", is(dataToSave.get(0).getBmi())))
                .andExpect(jsonPath("$.[0].wristCircumference", is(dataToSave.get(0).getWristCircumference())))
                .andExpect(jsonPath("$.[0].calfCircumference", is(dataToSave.get(0).getCalfCircumference())))
                .andExpect(jsonPath("$.[0].armCircumference", is(dataToSave.get(0).getArmCircumference())));
    }


    @Test
    void getShouldNotReturnDataIfNotFound() throws Exception{
        int patientId = 1;
        when(anthropometricDataDoctorCheckupService.get(patientId)).thenReturn(Optional.empty());

        mvc.perform(get("/doctor_checkup/" + patientId))
                .andExpect(status().isNotFound());

       // assertEquals(Optional.empty(), anthropometricDataDoctorCheckupService.get(patientId));
    }

    @Test
    void createShouldSaveData() throws Exception{
        int patientId = 1;
        Patient patient = validPatient(patientId);
        AnthropometricDataDoctorCheckup dataToSave = validData(patient);

        dataToSave.getPatient().setAnthropometricDataPatientCheckups(new ArrayList<AnthropometricDataPatientCheckup>());
        dataToSave.getPatient().setAnthropometricDataDoctorCheckups(new ArrayList<AnthropometricDataDoctorCheckup>());

        when(anthropometricDataDoctorCheckupService.save(dataToSave)).thenReturn(dataToSave);

        mvc.perform(post("/new_doctor_checkup/")
            .contentType(MediaType.APPLICATION_JSON)
            .content(objectMapper.writeValueAsBytes(dataToSave)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(dataToSave.getId())))
                .andExpect(jsonPath("$.patient.id", is(patientId)))
                .andExpect(jsonPath("$.weight", is(dataToSave.getWeight())))
                .andExpect(jsonPath("$.navelCircumference", is(dataToSave.getNavelCircumference())))
                .andExpect(jsonPath("$.hipsWaistline", is(dataToSave.getHipsWaistline())))
                .andExpect(jsonPath("$.waistCircumference", is(dataToSave.getWaistCircumference())))
                .andExpect(jsonPath("$.thighDxCircumference", is(dataToSave.getThighDxCircumference())))
                .andExpect(jsonPath("$.bmi", is(dataToSave.getBmi())))
                .andExpect(jsonPath("$.wristCircumference", is(dataToSave.getWristCircumference())))
                .andExpect(jsonPath("$.calfCircumference", is(dataToSave.getCalfCircumference())))
                .andExpect(jsonPath("$.armCircumference", is(dataToSave.getArmCircumference())));
    }


    private AnthropometricDataDoctorCheckup validData(Patient patient){

        LocalDate date = LocalDate.now();
        return AnthropometricDataDoctorCheckup.builder()
                .armCircumference(11.0)
                .bmi(11.0)
                .calfCircumference(14.2)
                .thighDxCircumference(178.0)
                .waistCircumference(14.3)
                .wristCircumference(74.3)
                .hipsWaistline(11.2)
                .navelCircumference(11.2)
                .weight(81.3)
                .date(date)
                .patient(patient)
                .build();
    }

    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }

}
