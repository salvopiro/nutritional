package nutritional.dati.antropometrici.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import nutritional.dati.antropometrici.domain.AnthropometricDataDoctorCheckup;
import nutritional.dati.antropometrici.domain.AnthropometricDataPatientCheckup;
import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.service.AnthropometricDataPatientCheckupService;
import nutritional.dati.antropometrici.service.PatientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(AnthropometricDataPatientCheckupController.class)
public class AnthropometricDataPatientCheckupControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private AnthropometricDataPatientCheckupService anthropometricDataPatientCheckupService;

    @MockBean
    private PatientService patientService;

    @Autowired
    private ObjectMapper objectMapper;

    @Test
    void getShouldReturnDataIfFound() throws Exception{
        int patientId =1;
        Patient patient = validPatient(patientId);
        AnthropometricDataPatientCheckup data1 = validData(patient);
        AnthropometricDataPatientCheckup data2 = validData(patient);

        List<AnthropometricDataPatientCheckup> dataList = new ArrayList<AnthropometricDataPatientCheckup>();

        dataList.add(data1);
        dataList.add(data2);

        patient.toBuilder().
                anthropometricDataPatientCheckups(dataList).
                build();

        when(anthropometricDataPatientCheckupService.get(patientId))
                .thenReturn(Optional.of(dataList));

        mvc.perform(get("/patient_checkup/" + patientId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(dataList.get(0).getId())))
                .andExpect(jsonPath("$.[0].patient.id", is(patientId)))
                .andExpect(jsonPath("$.[0].weight", is(dataList.get(0).getWeight())))
                .andExpect(jsonPath("$.[0].navelCircumference", is(dataList.get(0).getNavelCircumference())))
                .andExpect(jsonPath("$.[0].hipsWaistline", is(dataList.get(0).getHipsWaistline())));
    }

    @Test
    void getDataIfNotFound() throws Exception{
        int patientId = 1;

        when(anthropometricDataPatientCheckupService.get(patientId)).thenReturn(Optional.empty());

        mvc.perform(get("/patient_checkup/" + patientId))
                .andExpect(status().isNotFound());

//        assertEquals(
//                Optional.empty(),
//                anthropometricDataPatientCheckupService.get(patientId)
//        );
    }

    @Test
    void createShouldSaveData() throws Exception{
        int patientId = 1;

        Patient patient = validPatient(patientId);

        AnthropometricDataPatientCheckup anthropometricDataPatientCheckup = validData(patient);

        anthropometricDataPatientCheckup.getPatient().setAnthropometricDataDoctorCheckups(new ArrayList<AnthropometricDataDoctorCheckup>());
        anthropometricDataPatientCheckup.getPatient().setAnthropometricDataPatientCheckups(new ArrayList<AnthropometricDataPatientCheckup>());
        when(anthropometricDataPatientCheckupService
                .save(anthropometricDataPatientCheckup))
                .thenReturn(
                    anthropometricDataPatientCheckup
        );

        String valueAsString = objectMapper.writeValueAsString(anthropometricDataPatientCheckup);

        mvc.perform(post("/new_patient_checkup/")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(objectMapper.writeValueAsBytes(anthropometricDataPatientCheckup)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(anthropometricDataPatientCheckup.getId())))
                .andExpect(jsonPath("$.patient.id", is(anthropometricDataPatientCheckup.getPatient().getId())))
                .andExpect(jsonPath("$.weight", is(anthropometricDataPatientCheckup.getWeight())))
                .andExpect(jsonPath("$.navelCircumference", is(anthropometricDataPatientCheckup.getNavelCircumference())))
                .andExpect(jsonPath("$.hipsWaistline", is(anthropometricDataPatientCheckup.getHipsWaistline())));
    }



    private AnthropometricDataPatientCheckup validData(Patient patient){

        LocalDate date = LocalDate.now();
        return AnthropometricDataPatientCheckup.builder()
                .hipsWaistline(12.0)
                .navelCircumference(12.0)
                .weight(75.1)
                .patient(patient)
                .date(date)
                .build();
    }

    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }
}
