package nutritional.dati.antropometrici.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.service.PatientService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.stubbing.OngoingStubbing;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;


import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(PatientController.class)
public class PatientControllerTest {
    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    private PatientService patientService;

    @Test
    void getPatientIfFound()throws Exception{
        int patientId = 1;
        Patient patient = validPatient(patientId);

        when(patientService.get(patientId)).thenReturn(Optional.of(patient));


        mvc.perform(get("/patient/"+patientId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id",         is(patientId)))
                .andExpect((jsonPath("$.name",      is(patient.getName()))))
                .andExpect(jsonPath("$.surname",    is(patient.getSurname())))
                .andExpect(jsonPath("$.email",      is(patient.getEmail())))
                .andExpect(jsonPath("$.height",    is(patient.getHeight())));

    }

    @Test
    void getPatientIfNotFound() throws Exception{
        int patientId = 1;

        when(patientService.get(patientId)).thenReturn(Optional.empty());

        mvc.perform(get("/patient/"+patientId))
                .andExpect(status().isNotFound());

    }


    @Test
    void createShouldSavePatient() throws Exception{
        int patientId = 1;
        Patient patient = validPatient();

        when(patientService.save(patient))
                .thenReturn(
                        patient.toBuilder()
                                .id(patientId)
                                .build());

        mvc.perform(post("/new_patient")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(patient)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id",         is(patientId)))
                .andExpect(jsonPath("$.name",       is(patient.getName())))
                .andExpect(jsonPath("$.surname",    is(patient.getSurname())))
                .andExpect(jsonPath("$.email",      is(patient.getEmail())))
                .andExpect(jsonPath("$.height",     is(patient.getHeight())));
    }

    private Patient validPatient(){
        return Patient.builder()
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }


    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }
}
