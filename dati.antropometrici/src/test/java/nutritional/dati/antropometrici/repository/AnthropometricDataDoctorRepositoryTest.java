package nutritional.dati.antropometrici.repository;


import nutritional.dati.antropometrici.domain.AnthropometricDataDoctorCheckup;
import nutritional.dati.antropometrici.domain.Patient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class AnthropometricDataDoctorRepositoryTest {
    @Autowired
    AnthropometricDataDoctorCheckupRepository anthropometricDataDoctorCheckupRepository;

    @Autowired
    PatientRepository patientRepository;

    @Test
    public void findByPatientIdShouldReturnListOfData(){
        int patientId = 1;
        Patient patient = validPatient(patientId);

        AnthropometricDataDoctorCheckup anthropometricDataDoctorCheckup1 = validData(patient);
        AnthropometricDataDoctorCheckup anthropometricDataDoctorCheckup2 = validData(patient);

        patientRepository.save(patient);

        List<AnthropometricDataDoctorCheckup> dataList = new ArrayList<AnthropometricDataDoctorCheckup>();

        dataList.add(anthropometricDataDoctorCheckup1);
        dataList.add(anthropometricDataDoctorCheckup2);

        //controllo se il primo dato è stato salvato correttamente
        AnthropometricDataDoctorCheckup expectedData = anthropometricDataDoctorCheckupRepository.save(anthropometricDataDoctorCheckup1);
        assertEquals(expectedData, anthropometricDataDoctorCheckupRepository.findByPatientId(patientId).get().get(0));

        //controllo se sono stati salvati tutti e due i dati e vengono restituiti entrambi
        anthropometricDataDoctorCheckupRepository.save(anthropometricDataDoctorCheckup2);
        assertEquals(dataList, anthropometricDataDoctorCheckupRepository.findByPatientId(patientId).get());

    }

    @Test
    public void findByIdPatientShouldReturnEmptyIfNotFound(){
        assertEquals(anthropometricDataDoctorCheckupRepository.findByPatientId(124), Optional.empty());
    }


    private AnthropometricDataDoctorCheckup validData(Patient patient){

        LocalDate date = LocalDate.now();
        return AnthropometricDataDoctorCheckup.builder()
                .armCircumference(11.0)
                .bmi(11.0)
                .calfCircumference(14.2)
                .thighDxCircumference(178.0)
                .waistCircumference(14.3)
                .wristCircumference(74.3)
                .hipsWaistline(11.2)
                .navelCircumference(11.2)
                .weight(81.3)
                .date(date)
                .patient(patient)
                .build();
    }

    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }

}
