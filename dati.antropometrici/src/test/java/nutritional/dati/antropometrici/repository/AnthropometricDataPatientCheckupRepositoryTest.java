package nutritional.dati.antropometrici.repository;


import nutritional.dati.antropometrici.domain.AnthropometricDataPatientCheckup;
import nutritional.dati.antropometrici.domain.Patient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class AnthropometricDataPatientCheckupRepositoryTest {

    @Autowired
    AnthropometricDataPatientCheckupRepository anthropometricDataPatientCheckupRepository;

    @Autowired PatientRepository patientRepository;

    @Test
    public void findByPatientIdShouldReturnListOfData(){
        Patient patient = validPatient(1);
        AnthropometricDataPatientCheckup anthropometricDataPatientCheckup1 = validData(patient);
        AnthropometricDataPatientCheckup anthropometricDataPatientCheckup2 = validData((patient));


        patientRepository.save(patient);
        List<AnthropometricDataPatientCheckup> anthropometricDataPatientCheckupList = new ArrayList<AnthropometricDataPatientCheckup>();

        anthropometricDataPatientCheckupList.add(anthropometricDataPatientCheckup1);
        anthropometricDataPatientCheckupList.add(anthropometricDataPatientCheckup2);

        AnthropometricDataPatientCheckup expectedData = anthropometricDataPatientCheckupRepository.save(anthropometricDataPatientCheckup1);
        assertEquals(expectedData, anthropometricDataPatientCheckupRepository.findByPatientId(1).get().get(0) );

        anthropometricDataPatientCheckupRepository.save(anthropometricDataPatientCheckup2);

        assertEquals(anthropometricDataPatientCheckupList, anthropometricDataPatientCheckupRepository.findByPatientId(1).get());
    }


    @Test
    public void findByIdShouldReturnEmptyIfNotFound(){
        assertEquals(anthropometricDataPatientCheckupRepository.findByPatientId(124), Optional.empty());
    }

    private AnthropometricDataPatientCheckup validData(Patient patient){

        LocalDate date = LocalDate.now();
        return AnthropometricDataPatientCheckup.builder()
                .hipsWaistline(12.0)
                .navelCircumference(12.0)
                .weight(75.1)
                .patient(patient)
                .date(date)
                .build();
    }

    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }
}
