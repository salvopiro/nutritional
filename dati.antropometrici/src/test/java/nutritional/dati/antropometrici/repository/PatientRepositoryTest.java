package nutritional.dati.antropometrici.repository;


import nutritional.dati.antropometrici.domain.Patient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class PatientRepositoryTest {

    @Autowired
    PatientRepository patientRepository;

    @Test
    void findByIdShouldGetPatient(){
        int patientId = 1;
        Patient patientToSave = validPatient(patientId);

        Patient expectedPatient = patientRepository.save(patientToSave);

        assertEquals(expectedPatient, patientRepository.findById(expectedPatient.getId()).get());

    }

    @Test
    void findByIdShouldReturnEmptyIfPatientNotFound(){
        assertEquals(
                Optional.empty(),
                patientRepository.findById(1250)
        );
    }

    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }
}
