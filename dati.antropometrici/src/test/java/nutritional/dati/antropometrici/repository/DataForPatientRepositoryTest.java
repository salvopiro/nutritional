/*package nutritional.dati.antropometrici.repository;


import nutritional.dati.antropometrici.domain.DataForPatient;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.time.LocalDate;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;


@ExtendWith(SpringExtension.class)
@DataJpaTest
public class DataForPatientRepositoryTest {

    @Autowired
    private DataForPatientRepository dataForPatientRepository;


    @Test
    void findByPatientShouldGetDataForPatient(){
        int patientId = 1;
        LocalDate date1 = LocalDate.now();
        DataForPatient dataForPatientToSave =  DataForPatient.builder()
                .patient(patientId)
                .anthropometricData(1)
                .date(date1)
                .build();

        DataForPatient expectedDataForPatient = dataForPatientRepository.save(dataForPatientToSave);

        assertEquals(
                expectedDataForPatient,
                dataForPatientRepository.findByPatient(expectedDataForPatient.getPatient()).get()
                        .get(
                                dataForPatientRepository.findByPatient(expectedDataForPatient.getPatient())
                                        .get().size()-1  //indice dei dati antropometrici che ci aspettiamo
                )
        );
    }

    @Test
    void findByPatientShouldReturnEmptyIfDataForPatientNotFound(){
        assertEquals(
                Optional.empty(),
                dataForPatientRepository.findByPatient(452)
        );
    }
}
*/