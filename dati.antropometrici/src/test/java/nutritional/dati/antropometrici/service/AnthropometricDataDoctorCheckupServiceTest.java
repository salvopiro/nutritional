package nutritional.dati.antropometrici.service;


import nutritional.dati.antropometrici.domain.AnthropometricDataDoctorCheckup;
import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.repository.AnthropometricDataDoctorCheckupRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class AnthropometricDataDoctorCheckupServiceTest {

    @Mock
    AnthropometricDataDoctorCheckupRepository anthropometricDataDoctorCheckupRepository;

    @InjectMocks
    AnthropometricDataDoctorCheckupService anthropometricDataDoctorCheckupService;

    @Test
    public void getShouldReturnData(){
        int patientId = 1;
        Patient patient = validPatient(patientId);

        AnthropometricDataDoctorCheckup anthropometricDataDoctorCheckup1 = validData(patient);
        AnthropometricDataDoctorCheckup anthropometricDataDoctorCheckup2 = validData(patient);

        List<AnthropometricDataDoctorCheckup> dataList = new ArrayList<AnthropometricDataDoctorCheckup>();
        dataList.add(anthropometricDataDoctorCheckup1);
        dataList.add(anthropometricDataDoctorCheckup2);

        when(anthropometricDataDoctorCheckupRepository.findByPatientId(patientId)).thenReturn(Optional.of(dataList));

        assertEquals(dataList, anthropometricDataDoctorCheckupService.get(patientId).get());

    }

    @Test
    public void getShouldNotReturnData(){
        when(anthropometricDataDoctorCheckupRepository.findByPatientId(12)).thenReturn(Optional.empty());
        assertEquals(anthropometricDataDoctorCheckupService.get(12), Optional.empty());
    }

    private AnthropometricDataDoctorCheckup validData(Patient patient){

        LocalDate date = LocalDate.now();
        return AnthropometricDataDoctorCheckup.builder()
                .armCircumference(11.0)
                .bmi(11.0)
                .calfCircumference(14.2)
                .thighDxCircumference(178.0)
                .waistCircumference(14.3)
                .wristCircumference(74.3)
                .hipsWaistline(11.2)
                .navelCircumference(11.2)
                .weight(81.3)
                .date(date)
                .patient(patient)
                .build();
    }

    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }

}
