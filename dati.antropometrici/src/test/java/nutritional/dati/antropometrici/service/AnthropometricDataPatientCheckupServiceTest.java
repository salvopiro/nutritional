package nutritional.dati.antropometrici.service;


import nutritional.dati.antropometrici.domain.AnthropometricDataPatientCheckup;
import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.repository.AnthropometricDataPatientCheckupRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.mockito.Mockito.when;
import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(MockitoExtension.class)
public class AnthropometricDataPatientCheckupServiceTest {

    @Mock
    AnthropometricDataPatientCheckupRepository anthropometricDataPatientCheckupRepository;

    @InjectMocks
    AnthropometricDataPatientCheckupService anthropometricDataPatientCheckupService;

    @Test
    public void getShouldReturnData(){
        Patient patient = validPatient(1);
        AnthropometricDataPatientCheckup anthropometricDataPatientCheckup1 = validData(patient);
        AnthropometricDataPatientCheckup anthropometricDataPatientCheckup2 = validData(patient);

        List<AnthropometricDataPatientCheckup> anthropometricDataPatientCheckupList = new ArrayList<AnthropometricDataPatientCheckup>();
        anthropometricDataPatientCheckupList.add(anthropometricDataPatientCheckup1);
        anthropometricDataPatientCheckupList.add(anthropometricDataPatientCheckup2);

        when(anthropometricDataPatientCheckupRepository.findByPatientId(patient.getId())).thenReturn(Optional.of(anthropometricDataPatientCheckupList));

        assertEquals(anthropometricDataPatientCheckupList, anthropometricDataPatientCheckupService.get(patient.getId()).get());
    }

    @Test
    public void getShouldNotReturnData(){
        when(anthropometricDataPatientCheckupRepository.findByPatientId(12)).thenReturn(Optional.empty());
        assertEquals(anthropometricDataPatientCheckupService.get(12), Optional.empty());
    }

    private AnthropometricDataPatientCheckup validData(Patient patient){

        LocalDate date = LocalDate.now();
        return AnthropometricDataPatientCheckup.builder()
                .hipsWaistline(12.0)
                .navelCircumference(12.0)
                .weight(75.1)
                .patient(patient)
                .date(date)
                .build();
    }

    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }
}
