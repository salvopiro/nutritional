package nutritional.dati.antropometrici.service;


import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.repository.PatientRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class PatientServiceTest {

    @Mock
    PatientRepository patientRepository;

    @InjectMocks
    PatientService patientService;

    @Test
    void getShouldReturnPatient(){
        int patientId = 1;
        Patient patient = validPatient(patientId);

        when(patientRepository.findById(patientId)).thenReturn(Optional.of(patient));

        assertEquals(patient, patientService.get(patientId).get());

    }

    @Test
    void getShouldNotReturnPatient(){
        int patientId = 1;
        when(patientRepository.findById(patientId)).thenReturn(Optional.empty());

        assertTrue(patientService.get(patientId).isEmpty());
    }

    private Patient validPatient(int id){
        return Patient.builder()
                .id(id)
                .name("Mario")
                .surname("Rossi")
                .email("mariorossi@italia.it")
                .height(172.0)
                .build();
    }
}
