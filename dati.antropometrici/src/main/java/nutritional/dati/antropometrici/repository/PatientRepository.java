package nutritional.dati.antropometrici.repository;

import nutritional.dati.antropometrici.domain.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface PatientRepository extends CrudRepository<Patient, Integer> {
    public Optional<Patient> findByEmail(String email);
    public boolean existsByEmail(String email);

}
