package nutritional.dati.antropometrici.repository;

import nutritional.dati.antropometrici.domain.AnthropometricDataPatientCheckup;
import nutritional.dati.antropometrici.domain.Patient;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnthropometricDataPatientCheckupRepository extends
        CrudRepository<AnthropometricDataPatientCheckup, Integer> {

    Optional<List<AnthropometricDataPatientCheckup>> findByPatientId(int id);
}
