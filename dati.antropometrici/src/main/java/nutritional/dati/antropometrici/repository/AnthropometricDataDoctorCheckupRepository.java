package nutritional.dati.antropometrici.repository;

import nutritional.dati.antropometrici.domain.AnthropometricDataDoctorCheckup;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface AnthropometricDataDoctorCheckupRepository extends
        CrudRepository<AnthropometricDataDoctorCheckup, Integer> {
    Optional<List<AnthropometricDataDoctorCheckup>> findByPatientId(int id);
}
