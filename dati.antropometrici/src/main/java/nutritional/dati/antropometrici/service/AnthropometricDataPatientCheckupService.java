package nutritional.dati.antropometrici.service;

import nutritional.dati.antropometrici.domain.AnthropometricDataPatientCheckup;
import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.repository.AnthropometricDataPatientCheckupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnthropometricDataPatientCheckupService {

    private AnthropometricDataPatientCheckupRepository anthropometricDataPatientCheckupRepository;

    @Autowired
    public AnthropometricDataPatientCheckupService (
            AnthropometricDataPatientCheckupRepository anthropometricDataPatientCheckupRepository){
        this.anthropometricDataPatientCheckupRepository = anthropometricDataPatientCheckupRepository;
    }

    public Optional<List<AnthropometricDataPatientCheckup>> get (int id){
        return anthropometricDataPatientCheckupRepository.findByPatientId(id);
    }

    public AnthropometricDataPatientCheckup save(AnthropometricDataPatientCheckup anthropometricDataPatientCheckup){
        return anthropometricDataPatientCheckupRepository.save(anthropometricDataPatientCheckup);
    }
}
