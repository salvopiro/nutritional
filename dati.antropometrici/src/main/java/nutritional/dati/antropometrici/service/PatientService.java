package nutritional.dati.antropometrici.service;


import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.errors.PatientNotFoundException;
import nutritional.dati.antropometrici.repository.PatientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;


@Service
public class PatientService {

    PatientRepository patientRepository;

    @Autowired
    public PatientService(PatientRepository patientRepository){
        this.patientRepository = patientRepository;
    }


    public Optional<Patient> get(int id){
        return patientRepository.findById(id);
    }

    public Patient save(Patient patient) {
        return patientRepository.save(patient);
    }

    public Optional<Patient> findByEmail(String email){
        return patientRepository.findByEmail(email);
    }

    public boolean existsByEmail(String email){
        return patientRepository.existsByEmail(email);
    }
    public Optional<Patient> getByEmail(String email){
        return patientRepository.findByEmail(email);
    }
}
