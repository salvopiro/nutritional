package nutritional.dati.antropometrici.service;

import nutritional.dati.antropometrici.domain.AnthropometricDataDoctorCheckup;
import nutritional.dati.antropometrici.repository.AnthropometricDataDoctorCheckupRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class AnthropometricDataDoctorCheckupService {

    private AnthropometricDataDoctorCheckupRepository anthropometricDataDoctorCheckupRepository;

    @Autowired
    public AnthropometricDataDoctorCheckupService (
            AnthropometricDataDoctorCheckupRepository anthropometricDataDoctorCheckupRepository){
        this.anthropometricDataDoctorCheckupRepository = anthropometricDataDoctorCheckupRepository;
    }

    public Optional<List<AnthropometricDataDoctorCheckup>> get(int id){
        return anthropometricDataDoctorCheckupRepository.findByPatientId(id);
    }

    public AnthropometricDataDoctorCheckup save (AnthropometricDataDoctorCheckup anthropometricDataDoctorCheckup){
        return anthropometricDataDoctorCheckupRepository.save(anthropometricDataDoctorCheckup);
    }
}
