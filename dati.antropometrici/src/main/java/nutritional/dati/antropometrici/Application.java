package nutritional.dati.antropometrici;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import java.io.IOException;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

@EnableEurekaClient
@SpringBootApplication
public class Application {
	private static final Logger LOGGER = LoggerFactory.getLogger(Application.class);
	public static void main(String[] args) {

		Path dir = Paths.get("~/logprogettoscs3");
		try {
			DirectoryStream<Path> ds = Files.newDirectoryStream(dir, "*");
			for (Path entry: ds) {
				getLogger().info(entry.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		SpringApplication.run(Application.class, args);
	}
	public static Logger getLogger(){
		if(LOGGER != null)
			return LOGGER;
		return null;
	}

}
