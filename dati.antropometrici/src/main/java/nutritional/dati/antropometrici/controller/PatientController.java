package nutritional.dati.antropometrici.controller;

import nutritional.dati.antropometrici.Application;
import nutritional.dati.antropometrici.domain.Patient;
import nutritional.dati.antropometrici.errors.PatientNotFoundException;
import nutritional.dati.antropometrici.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class PatientController {

    private PatientService patientService;

    @Autowired
    public PatientController (PatientService patientService){
        this.patientService = patientService;
    }


    @GetMapping("/patient/{id}")
    public Patient getPatient(@PathVariable int id, @RequestHeader("authentication") String jwt){
        return patientService
                .get(id)
                .orElseThrow(PatientNotFoundException::new);
    }

    @GetMapping("/patient_email/{email}")
    public Patient getByEmail(@PathVariable String email){
        return patientService.getByEmail(email)
                .orElseThrow(PatientNotFoundException::new);
    }

    @GetMapping("/exists_patient_email/{email}")
    public boolean existsByEmail(@PathVariable String email){
        return patientService.existsByEmail(email);
    }

    @PostMapping("/new_patient/")
    @ResponseStatus(HttpStatus.CREATED)
    Patient postPatient (@RequestBody Patient patient){
        Application.getLogger().info("Creating new patient: " + patient.getName() + " " + patient.getSurname());
        return patientService.save(patient);
    }
}
