package nutritional.dati.antropometrici.controller;

import nutritional.dati.antropometrici.Application;
import nutritional.dati.antropometrici.domain.AnthropometricDataPatientCheckup;
import nutritional.dati.antropometrici.errors.AnthropometricDataPatientCheckupException;
import nutritional.dati.antropometrici.service.AnthropometricDataPatientCheckupService;
import nutritional.dati.antropometrici.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AnthropometricDataPatientCheckupController {

    AnthropometricDataPatientCheckupService anthropometricDataPatientCheckupService;


    //@Autowired
   // PatientService patientService;

    @Autowired
    public AnthropometricDataPatientCheckupController(AnthropometricDataPatientCheckupService anthropometricDataPatientCheckupService){
        this.anthropometricDataPatientCheckupService = anthropometricDataPatientCheckupService;
        //this.patientService = patientService;
    }

    @GetMapping("/patient_checkup/{id}")
    List<AnthropometricDataPatientCheckup> get (@PathVariable int id){
        return anthropometricDataPatientCheckupService.get(id)
                .orElseThrow(AnthropometricDataPatientCheckupException::new);
    }

    @PostMapping("/new_patient_checkup/")
    @ResponseStatus(HttpStatus.CREATED)
    AnthropometricDataPatientCheckup save(@RequestBody AnthropometricDataPatientCheckup anthropometricDataPatientCheckup){
        Application.getLogger().info("Creating new patient checkup for the patient: " + anthropometricDataPatientCheckup.getPatient().getName()+ " " + anthropometricDataPatientCheckup.getPatient().getSurname());
        return anthropometricDataPatientCheckupService.save(anthropometricDataPatientCheckup);
    }
}
