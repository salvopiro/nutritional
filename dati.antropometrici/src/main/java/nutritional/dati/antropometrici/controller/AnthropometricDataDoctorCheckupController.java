package nutritional.dati.antropometrici.controller;


import nutritional.dati.antropometrici.Application;
import nutritional.dati.antropometrici.domain.AnthropometricDataDoctorCheckup;
import nutritional.dati.antropometrici.errors.AnthropometricDataDoctorCheckupException;
import nutritional.dati.antropometrici.service.AnthropometricDataDoctorCheckupService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class AnthropometricDataDoctorCheckupController {


    AnthropometricDataDoctorCheckupService anthropometricDataDoctorCheckupService;

    @Autowired
    public AnthropometricDataDoctorCheckupController (AnthropometricDataDoctorCheckupService anthropometricDataDoctorCheckupService){
        this.anthropometricDataDoctorCheckupService = anthropometricDataDoctorCheckupService;
    }

    @GetMapping("/doctor_checkup/{id}")
    List<AnthropometricDataDoctorCheckup> get(@PathVariable int id){
        return anthropometricDataDoctorCheckupService.get(id).orElseThrow(AnthropometricDataDoctorCheckupException::new);
    }

    @PostMapping("/new_doctor_checkup/")
    @ResponseStatus(HttpStatus.CREATED)
    public AnthropometricDataDoctorCheckup post(@RequestBody AnthropometricDataDoctorCheckup anthropometricDataDoctorCheckup){
        Application.getLogger().info("Creating new  doctor checkup for the patient: "+ anthropometricDataDoctorCheckup.getPatient().getName() + " " + anthropometricDataDoctorCheckup.getPatient().getSurname());
        return anthropometricDataDoctorCheckupService.save(anthropometricDataDoctorCheckup);
    }

}



//    @Autowired
//    PatientService patientService;
