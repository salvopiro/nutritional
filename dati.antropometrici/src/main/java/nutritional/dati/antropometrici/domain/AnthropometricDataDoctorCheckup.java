package nutritional.dati.antropometrici.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.Set;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
public class AnthropometricDataDoctorCheckup {
    @Id
    @GeneratedValue
    private int id;

    @ManyToOne
    private Patient patient;

    private double waistCircumference;  		//circonferenza vita
    private double thighDxCircumference;		//circonferenza coscia dx
    private double bmi; 						//indice di massa corporea

    private double wristCircumference;			//cirocnferenza polso
    private double calfCircumference;			//circonferenza polpaccio
    private double armCircumference;			//circonferenza braccio

    private double weight;  					// peso
    private double navelCircumference;			// circonferenza ombelico
    private double hipsWaistline;				// circonferenza fianchi

    private LocalDate date;
}
