package nutritional.dati.antropometrici.domain;


import lombok.*;
import org.hibernate.annotations.NaturalId;

import javax.persistence.*;
import javax.validation.constraints.Email;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
public class Patient {


    @Id
    @GeneratedValue
    @Column
    private Integer id;

    private String name;

    private String surname;

    @Email
    @NaturalId
    private String email;

    private double height;

}
