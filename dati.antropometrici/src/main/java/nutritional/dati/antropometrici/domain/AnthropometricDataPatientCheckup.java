package nutritional.dati.antropometrici.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder(toBuilder = true)
public class AnthropometricDataPatientCheckup {

    @Id
    @GeneratedValue
    @Column(name = "id")
    private int id;

    @ManyToOne
    private Patient patient;

    private double weight;  					// peso
    private double navelCircumference;			// circonferenza ombelico
    private double hipsWaistline;				// circonferenza fianchi


    private LocalDate date;
}
