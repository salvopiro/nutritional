package nutritional.dishes.repository;

import nutritional.dishes.domain.Dish;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface DishesRepository extends CrudRepository<Dish, Integer> {
}
