package nutritional.dishes.service;

import nutritional.dishes.domain.Dish;
import nutritional.dishes.event.Event;
import nutritional.dishes.event.EventDispatcher;
//import nutritional.dishes.event.NewDishEvent;
import nutritional.dishes.repository.DishesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class DishService {

    private DishesRepository dishesRepository;

    @Autowired
    EventDispatcher eventDispatcher;

    @Autowired
    public DishService (DishesRepository dishesRepository){
        this.dishesRepository = dishesRepository;
    }

    public List<Dish> getAllDishes(){
        return  (List<Dish>) dishesRepository.findAll();

    }

    public Dish save(Dish dish){

        Dish dishSaved = dishesRepository.save(dish);
        eventDispatcher.send(new Event(dishSaved.getName(), "new dish"));
        return dishSaved;
    }

    public Optional<Dish> getDishById(int id){
        return  dishesRepository.findById(id);
    }
}
