package nutritional.dishes.service;

import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.PutObjectRequest;
import lombok.AllArgsConstructor;
import nutritional.dishes.DishesApplication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;


@AllArgsConstructor
@Service
public class AmazonS3Service {

    @Autowired
    private final AmazonS3 amazonS3;

    @Scheduled(cron = "15 20 11 * * ?", zone = "Europe/Rome")
    public void upload(){

        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
        Date date = new Date();
        String today = formatter.format(date);

        File f = new File("./log/dishes.log");
        File copied = new File("./log/dishes"+today+".log");
        try {


            InputStream in = new BufferedInputStream(new FileInputStream(f));
            OutputStream out = new BufferedOutputStream(new FileOutputStream(copied));

            byte[] buffer = new byte[1024];
            int lengthRead;
            while ((lengthRead = in.read(buffer)) > 0) {
                out.write(buffer, 0, lengthRead);
                out.flush();
            }

            amazonS3.putObject(new PutObjectRequest("logprogettosc", "dishes/dishes"+today+".log", copied));
        } catch (Exception e) {
            DishesApplication.getLogger().error("Failed to upload the file", e);
        }
        copied.delete();

    }

}
