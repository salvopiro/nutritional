package nutritional.dishes.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder(toBuilder = true)
@Entity
public class Dish {

    @Id
    @GeneratedValue
    @Column
    private int id;

    private String name;

    private double calories;
    // proteine lipidi caboidrati fibra
    private double proteins;

    private double lipids;

    private double carbohydrates;

    private double fibers;

    @Lob
    @Column(length = 1024)
    private String process;
}
