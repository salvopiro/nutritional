package nutritional.dishes.event;


import nutritional.dishes.DishesApplication;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class EventDispatcher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    private Binding binding;

    public void send (Event event) {
        DishesApplication.getLogger().info("New tips of the week. Sending the message on the rabbitmq queue in the exchange: "
                +binding.getExchange()+ " with the routing key: "+ binding.getRoutingKey());
        rabbitTemplate.convertAndSend(binding.getExchange(), binding.getRoutingKey(), event);
    }
}