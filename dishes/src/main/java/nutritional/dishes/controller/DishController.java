package nutritional.dishes.controller;

import nutritional.dishes.domain.Dish;
import nutritional.dishes.errors.DishNotFoundException;
import nutritional.dishes.service.DishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
public class DishController {

    private DishService dishService;

    @Autowired
    public DishController (DishService dishService){
        this.dishService = dishService;
    }

    @GetMapping("/all_dish")
    public List<Dish> getAllDishes (){
        return dishService.getAllDishes();
    }

    @GetMapping("/dish/{id}")
    public Dish getDishById(@PathVariable int id){
        return dishService.getDishById(id).orElseThrow(DishNotFoundException::new);
    }

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping("/new/")
    public Dish saveNewDish(@RequestBody Dish dish){
        return dishService.save(dish);
    }

}
