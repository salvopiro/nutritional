package nutritional.dishes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@EnableEurekaClient
@SpringBootApplication
public class DishesApplication {
	private static final Logger LOGGER = LoggerFactory.getLogger(DishesApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DishesApplication.class, args);
	}

	public static Logger getLogger(){
		if(LOGGER != null)
			return LOGGER;
		return null;
	}

}
