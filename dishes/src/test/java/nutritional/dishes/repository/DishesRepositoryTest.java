package nutritional.dishes.repository;


import nutritional.dishes.domain.Dish;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;


import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertEquals;

@ExtendWith(SpringExtension.class)
@DataJpaTest
public class DishesRepositoryTest {

    @Autowired
    DishesRepository dishesRepository;

    @Test
    public void findByIdShouldReturnDishIfFound(){
        Dish dish = validDish();
        Dish dishSaved = dishesRepository.save(dish);
        assertEquals(dishSaved, dishesRepository.findById(1).get());
    }


    @Test
    public void findByIdShouldNotReturnDishIfNotFound(){
        assertEquals(Optional.empty(), dishesRepository.findById(1));
    }

    public Dish validDish(){
        return Dish.builder()
                .name("Ricetta")
                .process("procedimento ricetta")
                .calories(12.0)
                .carbohydrates(15.0)
                .fibers(45.1)
                .lipids(71.1)
                .proteins(12.0)
                .build();
    }

}
