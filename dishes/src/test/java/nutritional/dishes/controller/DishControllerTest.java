package nutritional.dishes.controller;


import com.fasterxml.jackson.databind.ObjectMapper;
import nutritional.dishes.domain.Dish;
import nutritional.dishes.service.DishService;
import org.hamcrest.Matcher;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(DishController.class)
public class DishControllerTest {

    @Autowired
    MockMvc mvc;

    @Autowired
    ObjectMapper objectMapper;

    @MockBean
    DishService dishService;

    @Test
    public void getDishByIdShouldReturnDishIfFound() throws Exception{
        int dishId = 1;
        Dish dish = validDish(dishId);

        when(dishService.getDishById(dishId)).thenReturn(Optional.of(dish));

        mvc.perform(get("/dish/"+dishId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.id", is(dishId)))
                .andExpect(jsonPath("$.name", is(dish.getName())))
                .andExpect(jsonPath("$.process", is(dish.getProcess())))
                .andExpect(jsonPath("$.calories", is(dish.getCalories())))
                .andExpect(jsonPath("$.carbohydrates", is(dish.getCarbohydrates())))
                .andExpect(jsonPath("$.fibers", is(dish.getFibers())))
                .andExpect(jsonPath("$.lipids", is(dish.getLipids())))
                .andExpect(jsonPath("$.proteins", is(dish.getProteins())));

    }

    @Test
    public void getAllDishesShouldReturnAllDishesIfFound() throws Exception{
        Dish dish1 = validDish(1);
        Dish dish2 = validDish(2);

        List<Dish> dishList = new ArrayList<Dish>();
        dishList.add(dish1);
        dishList.add(dish2);
        when(dishService.getAllDishes()).thenReturn(dishList);

        mvc.perform(get("/dishes"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].id", is(dishList.get(0).getId())))
                .andExpect(jsonPath("$.[1].id", is(dishList.get(1).getId())));
    }

    @Test
    public void getDishByIdShouldNotReturnDishIfNotFound() throws Exception{
        when(dishService.getDishById(1)).thenReturn(Optional.empty());
        mvc.perform(get("/dish/1")).andExpect(status().isNotFound());
    }

    @Test
    public void postShouldSaveDish()throws Exception{
        Dish dish = validDish();

        when(dishService.save(dish)).thenReturn(dish.toBuilder().id(0).build());

        mvc.perform(post("/new_dish")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsBytes(dish)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.id", is(dish.getId())))
                .andExpect(jsonPath("$.name", is(dish.getName())))
                .andExpect(jsonPath("$.process", is(dish.getProcess())))
                .andExpect(jsonPath("$.calories", is(dish.getCalories())))
                .andExpect(jsonPath("$.carbohydrates", is(dish.getCarbohydrates())))
                .andExpect(jsonPath("$.fibers", is(dish.getFibers())))
                .andExpect(jsonPath("$.lipids", is(dish.getLipids())))
                .andExpect(jsonPath("$.proteins", is(dish.getProteins())));
    }

    public Dish validDish(int id){
        return Dish.builder()
                .id(id)
                .name("Ricetta")
                .process("procedimento ricetta")
                .calories(12.0)
                .carbohydrates(15.0)
                .fibers(45.1)
                .lipids(71.1)
                .proteins(12.0)
                .build();
    }

    public Dish validDish(){
        return Dish.builder()
                .name("Ricetta")
                .process("procedimento ricetta")
                .calories(12.0)
                .carbohydrates(15.0)
                .fibers(45.1)
                .lipids(71.1)
                .proteins(12.0)
                .build();
    }

}
