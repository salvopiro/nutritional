package nutritional.dishes.service;


import nutritional.dishes.domain.Dish;
import nutritional.dishes.repository.DishesRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Optional;

import static org.mockito.Mockito.when;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;


@ExtendWith(MockitoExtension.class)
public class DishServiceTest {

    @Mock
    DishesRepository dishesRepository;

    @InjectMocks
    DishService dishService;

    @Test
    public void getShouldReturnDish(){

        Dish dish = validDish(1);

        when(dishesRepository.findById(1)).thenReturn(Optional.of(dish));

        assertEquals(dish, dishService.getDishById(1).get());
    }

    @Test
    public void getShouldNotReturnDish(){
        when(dishesRepository.findById(1)).thenReturn(Optional.empty());
        assertTrue(dishService.getDishById(1).isEmpty());
    }
    public Dish validDish(int id){
        return Dish.builder()
                .id(id)
                .name("Ricetta")
                .process("procedimento ricetta")
                .calories(12.0)
                .carbohydrates(15.0)
                .fibers(45.1)
                .lipids(71.1)
                .proteins(12.0)
                .build();
    }
}
