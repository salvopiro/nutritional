package nutitional.mailservice.event;

import nutitional.mailservice.MailserviceApplication;
import org.springframework.amqp.core.Binding;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
public class EventDispatcher {

    @Autowired
    private RabbitTemplate rabbitTemplate;

    @Autowired
    Binding bindingRequestMail;

    public void send (RequestUsersEmail requestUsersEmail) {
        MailserviceApplication.getLogger().info("Require the email of user with a message on the rabbitmq queue");
        rabbitTemplate.convertAndSend(bindingRequestMail.getExchange(),
                bindingRequestMail.getRoutingKey(), requestUsersEmail);
    }
}
