package nutitional.mailservice.event;


import lombok.extern.slf4j.Slf4j;
import nutitional.mailservice.MailserviceApplication;
import nutitional.mailservice.mail.MailServiceImpl;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

@Slf4j
@Component
public class EventHandler {

    @Autowired
    private MailServiceImpl mailService;

    @Autowired
    private EventDispatcher eventDispatcher;

    @RabbitListener(queues = "${event.queue}")
    void handleNewDish(Event event) {
        MailserviceApplication.getLogger().info("Receive new dish event with subject : " + event.getSubject() + "\n and text : " + event.getText());
        eventDispatcher.send(new RequestUsersEmail(event.getSubject(), event.getText()));
    }

    @RabbitListener(queues = "${mail.queue}")
    void handleRequestUsersEmail(ResponseUsersEmail responseUsersEmail){

        MailserviceApplication.getLogger().info("Receive message containing the list of email of the users");
        List<String> emailList = responseUsersEmail.getEmailUsersList();
        if(emailList.isEmpty()) MailserviceApplication.getLogger().info("No users email exist");
        else {
            MailserviceApplication.getLogger().info("I'm sending the email of new "+responseUsersEmail.getSubject() + " at the users");
            emailList.stream().forEach(email -> mailService.sendSimpleMessage(
                    email, responseUsersEmail.getSubject(), responseUsersEmail.getText()));
            }
    }
}
