package nutitional.mailservice.mail;

public interface MailService {

    void sendSimpleMessage(String to,
                           String subject,
                           String text);
}
