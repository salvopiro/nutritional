package nutitional.mailservice.configuration;


import org.springframework.amqp.core.Binding;
import org.springframework.amqp.core.BindingBuilder;
import org.springframework.amqp.core.Queue;
import org.springframework.amqp.core.TopicExchange;
import org.springframework.amqp.rabbit.annotation.RabbitListenerConfigurer;
import org.springframework.amqp.rabbit.listener.RabbitListenerEndpointRegistrar;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.messaging.converter.MappingJackson2MessageConverter;
import org.springframework.messaging.handler.annotation.support.DefaultMessageHandlerMethodFactory;
import org.springframework.stereotype.Component;


@Configuration
public class RabbitMQConfiguration{

    @Bean
    public TopicExchange topicExchangeRequest(@Value("${exchange.request}") final String exchangeName){
        return new TopicExchange(exchangeName);
    }

    @Bean
    public Queue queueRequest(@Value("${mail.queue}") final String queueName){
        return new Queue(queueName, false);
    }

    @Bean
    public Binding bindingRequestMail(Queue queueRequest, TopicExchange topicExchangeRequest,
                                      @Value("${request.routing-key}") String routingKey){
        return BindingBuilder.bind(queueRequest).to(topicExchangeRequest).with(routingKey);
    }

    @Bean
    public TopicExchange topicExchangeNew(@Value("${exchange.new}") final String exchangeName){
        return new TopicExchange(exchangeName);
    }


    @Bean
    public Queue queueEvent(@Value("${event.queue}") final String queueName){
        return new Queue(queueName, false);
    }

    @Bean
    public Binding bindingEvent(Queue queueEvent, TopicExchange topicExchangeNew,
                                @Value("${new.routing-key}") String routingKey){

        return BindingBuilder.bind(queueEvent).to(topicExchangeNew).with(routingKey);
    }


    @Bean
    public Jackson2JsonMessageConverter producerJackson2MessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

}

