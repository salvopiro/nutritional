package nutitional.mailservice.controller;


import nutitional.mailservice.mail.MailServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MailController {

    @Autowired
    MailServiceImpl mailService;

    @GetMapping("/send/{to}")
    public String sendTestEmail(@PathVariable String to){
        mailService.sendSimpleMessage(to, "Test", "test");
        return "OK";
    }
}
